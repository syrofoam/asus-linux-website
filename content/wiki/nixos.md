+++
title = "Asusctl and Supergfxctl on NixOS"
description = "A simple guide for getting asusctl and supergfxctl running on NixOS"
sort_by = "none"
template = "page/wiki.html"
author = "Carlos Campos Herrera"
+++

# Supergfxctl and Asusctl on NixOS

This guide expects some previous knowledge about NixOS and
it's configuration system. 

Please note that NixOS in not officially supported by these projects,
and any issues specific to them shall be reported on the nixpkgs's 
[GitHub page](https://github.com/NixOS/nixpkgs/issues).

# Supergfxctl

## Installation

In order to install supergfxctl add to your configuration:

```nix
services.supergfxd.enable = true;
```

## Verification

Recently, there were some issues with this module
not allowing supergfxctl to detect the graphics card.

Try running `supergfxctl -S` 

If it fails know that a fix is on the way, but until it gets merged you can add this line to make it work:

```nix
systemd.services.supergfxd.path = [ pkgs.pciutils ];
```


# Asusctl and ROG control center

## Installation

ROG Control Center is included in the asusctl module, so you only have to add to the configuration file:

```nix
services = {
    asusd = {
      enable = true;
      enableUserService = true;
    };
}
```

{{
  section_contribute(
    head="Has this guide helped you set up your machine?"
    text=true
    donate=true
  )
}}